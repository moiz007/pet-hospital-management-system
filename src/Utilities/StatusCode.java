/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilities;

/**
 *
 * @author moiza
 */
public class StatusCode {
    
    public static final int FILE_NOT_EXISTS = 1;
    
    public static final int PATIENT_NOT_EXISTS = 2;
    
    public static final int PATIENT_EXISTS_FOR_TODAY = 3;
    
    
}
