/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lovelypethms.Entities;

/**
 *
 * @author moiza
 */

public class InPatient extends Patient{
    
    Room room;
    
    public InPatient(Room room){
    this.room = room;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
}
