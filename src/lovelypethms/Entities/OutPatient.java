/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lovelypethms.Entities;

/**
 *
 * @author moiza
 */
public class OutPatient extends Patient{
    public OutPatient(){
    }
    
    public OutPatient(Integer patientCode,String patientType,Integer day,Double fee){
        super(patientCode,patientType,day,fee);
    }

    public Integer getPatientCode() {
        return super.patientCode;
    }

 

    public String getPatientType() {
        return super.patientType;
    }



    public Integer getDay() {
        return super.day;
    }



    public Double getFee() {
        return super.fee;
    }

    
    
}
