/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lovelypethms.Entities;


public class Patient {

    Integer patientCode;
    
    String patientType;
    
    Integer day;
    
    Double fee;
    
    public Patient(){
    
    }
     
    public Patient(Integer patientCode,String patientType,Integer day,Double fee){
    this.patientCode = patientCode;
    this.patientType = patientType;
    this.day = day;
    this.fee = fee;
    }

    public Integer getPatientCode() {
        return patientCode;
    }

    public void setPatientCode(Integer patientCode) {
        this.patientCode = patientCode;
    }

    public String getPatientType() {
        return patientType;
    }

    public void setPatientType(String patientType) {
        this.patientType = patientType;
    }   

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    
       
}
