package lovelypethms.forms;
import Utilities.StatusCode;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
public class AddPatient extends javax.swing.JFrame {
    public AddPatient() {
        initComponents();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        patientCodeEditText = new javax.swing.JTextField();
        patientTypeComboBox = new javax.swing.JComboBox<>();
        patientRoomComboBox = new javax.swing.JComboBox<>();
        feeEditText = new javax.swing.JTextField();
        addButton = new javax.swing.JButton();
        nameEditText = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Add Patient");
        setLocation(new java.awt.Point(550, 200));
        setResizable(false);

        jLabel1.setText("Patient Code");

        jLabel2.setText("Patient Type");

        jLabel3.setText("Room");

        jLabel4.setText("Fee");

        patientTypeComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "---Select Patient Type---", "OutPatient", "Hospitalisation" }));
        patientTypeComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                patientTypeComboBoxItemStateChanged(evt);
            }
        });

        patientRoomComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "---Select Room---", "A", "B", "C" }));

        addButton.setText("Add Patient");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });

        jLabel5.setText("Name");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(220, 220, 220)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(addButton)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addGap(26, 26, 26)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(patientCodeEditText)
                            .addComponent(patientTypeComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(patientRoomComboBox, 0, 148, Short.MAX_VALUE)
                            .addComponent(feeEditText)
                            .addComponent(nameEditText))
                        .addGap(9, 9, 9)))
                .addContainerGap(237, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(92, 92, 92)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(patientCodeEditText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nameEditText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(patientTypeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(patientRoomComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(feeEditText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(addButton)
                .addContainerGap(85, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void patientTypeComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_patientTypeComboBoxItemStateChanged
        // TODO add your handling code here:
        if(patientTypeComboBox.getSelectedItem().equals(
        patientTypeComboBox.getItemAt(1))){
            patientRoomComboBox.setEnabled(false);
        }
        else{
        patientRoomComboBox.setEnabled(true);
        }
       
    }//GEN-LAST:event_patientTypeComboBoxItemStateChanged

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed

        if(isDataProvided()){
            Calendar calendar = Calendar.getInstance();
            int today = calendar.get(Calendar.DAY_OF_MONTH);
            String patientCode = patientCodeEditText.getText().toString();
            
            int status = isPatientExistsInFile(String.valueOf(today),patientCode);
            
            if(status == StatusCode.PATIENT_EXISTS_FOR_TODAY){
                JOptionPane.showMessageDialog(null,"Pet with code "+patientCodeEditText.getText().toString()+""
                        + " already exists ");
            }
            else{
                
                String pCode = patientCodeEditText.getText().toString();
                
                String pName = nameEditText.getText().toString();
                
                String pType = patientTypeComboBox.getSelectedItem().toString();
                
                String price = feeEditText.getText().toString();
                
                String log = today+" "+pCode+" "+pName+" "+pType+" "+price;
                
                if(patientRoomComboBox.isEnabled()){
                    String room = patientRoomComboBox.getSelectedItem().toString();
                    log = log + " "+room;
                }
                
                try {
                    BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("log.txt",true));
                    bufferedWriter.write(log);
                    bufferedWriter.newLine();
                    bufferedWriter.close();
                    
                    JOptionPane.showMessageDialog(null,"Patient Added Successfully! ");
                    clearForm();
                
                } 
                catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
                
                // patient does not exists , so add the patient 
            }
            
            // make sure whether this record does not exists in file.
            
        }
    }//GEN-LAST:event_addButtonActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AddPatient().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JTextField feeEditText;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JTextField nameEditText;
    private javax.swing.JTextField patientCodeEditText;
    private javax.swing.JComboBox<String> patientRoomComboBox;
    private javax.swing.JComboBox<String> patientTypeComboBox;
    // End of variables declaration//GEN-END:variables

    
    private boolean isDataProvided() {
    if(patientCodeEditText.getText().toString().equals("")){
       JOptionPane.showMessageDialog(null, "Enter Pet Code");
       return false;
    }
    else if(nameEditText.getText().toString().equals("")){
    JOptionPane.showMessageDialog(null,"Enter Pet Name");
    return false;
    }
    else if(patientTypeComboBox.getSelectedItem().equals(
    patientTypeComboBox.getItemAt(0))){
    JOptionPane.showMessageDialog(null,"Select Patient Type");
    return false;
    }
    else if(patientRoomComboBox.getSelectedItem().equals(
    patientRoomComboBox.getItemAt(0)) && patientRoomComboBox.isEnabled()){
    JOptionPane.showMessageDialog(null,"Select Room Number ");
    return false;
    }
    else if(feeEditText.getText().toString().equals("")){
        JOptionPane.showMessageDialog(null,"Enter Price");
        return false;
    }
    return true;
    }

    public int isPatientExistsInFile(String currentDay,String patientCode){
        try{
        File file = new File("log.txt");
        if(file.exists()){
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            StringBuffer sb = new StringBuffer();
            String line;
            while((line=bufferedReader.readLine()) != null){
            if(line.split(" ")[0].equals(currentDay) && line.split(" ")[1].equals(patientCode)){
              
                 bufferedReader.close();
                 return StatusCode.PATIENT_EXISTS_FOR_TODAY;
            }
           }
           bufferedReader.close();
        }
        else{ 
            return StatusCode.FILE_NOT_EXISTS;
        }
        
        }
        catch(Exception e){
            
        }
        
        return StatusCode.PATIENT_NOT_EXISTS;
    }

    private void clearForm() {
        patientCodeEditText.setText("");
        nameEditText.setText("");
        patientTypeComboBox.setSelectedIndex(0);
        patientRoomComboBox.setSelectedIndex(0);
        feeEditText.setText("");
    }

    
    
}