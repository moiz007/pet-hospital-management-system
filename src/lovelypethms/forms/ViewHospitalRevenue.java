/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lovelypethms.forms;

import java.io.BufferedReader;
import java.io.FileReader;
import javax.swing.JLabel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author moiza
 */
public class ViewHospitalRevenue extends javax.swing.JFrame {

    /**
     * Creates new form ViewHospitalRevenue
     */
    public ViewHospitalRevenue() {
        initComponents();
        netTotal.setVisible(false);
        calculateHospitalRevenue();
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        closeButton = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        revenueTable = new javax.swing.JTable();
        netTotal = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Hospital Revenue");
        setLocation(new java.awt.Point(50, 200));
        setResizable(false);

        jLabel1.setText("Net Total");

        closeButton.setText("Close");

        revenueTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Day", "Pet Code", "Pet Name", "Patient Type", "Room If Any", "Fee", "Discharge Day", "Total Fee"
            }
        ));
        jScrollPane2.setViewportView(revenueTable);

        netTotal.setText("jLabel2");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(677, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(30, 30, 30)
                        .addComponent(netTotal))
                    .addComponent(closeButton, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(11, 11, 11)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 773, Short.MAX_VALUE)
                    .addGap(11, 11, 11)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(383, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(netTotal))
                .addGap(18, 18, 18)
                .addComponent(closeButton, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 361, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(87, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ViewHospitalRevenue.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ViewHospitalRevenue.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ViewHospitalRevenue.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ViewHospitalRevenue.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ViewHospitalRevenue().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton closeButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel netTotal;
    private javax.swing.JTable revenueTable;
    // End of variables declaration//GEN-END:variables

    private void calculateHospitalRevenue() {
        Integer roomAFee = 10;
        Integer roomBFee = 7;
        Integer roomCFee = 5;
        
        
        Integer netRev = 0;
        
        DefaultTableModel model = (DefaultTableModel) revenueTable.getModel();
        
        try{
            BufferedReader bf = new BufferedReader(new FileReader("discharge.txt"));
            String line;
            Integer i = 0;
            while((line = bf.readLine()) != null){
                String words[] = line.split("\\s+");
                Integer total = 0;
                Integer dischargeDay=0;
                if(words.length == 7){
                    dischargeDay = Integer.parseInt(words[6]);
                    Integer inDay = Integer.parseInt(words[0]);
                    Integer fee = Integer.parseInt(words[4]);
                    String room = words[5];
                    Integer noOfDays = dischargeDay - inDay;
                    // Room A -> $10 , Room B -> $7, Room C -> $5 
                    if(room.equals("A")){
                        total = noOfDays * (fee + roomAFee);
                    }
                    else if(room.equals("B")){
                        total = noOfDays * (fee + roomBFee);
                    }
                    else{
                        total = noOfDays * (fee + roomCFee);
                    }
                    
                    model.insertRow(i,new Object[]{words[0],words[1],words[2],words[3],words[5],words[4],dischargeDay+"",total+""});
                   
                   
                }
                else{
                dischargeDay = Integer.parseInt(words[5]);
                total = Integer.parseInt(words[4]);
                model.insertRow(i,new Object[]{words[0],words[1],words[2],words[3],"Null",words[4],dischargeDay+"",total+""});
                }
                netRev = netRev + total;        
            }
            bf.close();
        }
        catch(Exception e){
            System.out.println("Exception: "+e.getMessage());
        }
        
        netTotal.setVisible(true);
        netTotal.setText(netRev+" $ ");
        System.out.println(netTotal.getText().toString());
        
    }
}
